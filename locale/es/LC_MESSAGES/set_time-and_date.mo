��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �  �    8   �     
       !   &  !   H     j     p     �     �  4   �                       	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:04+0000
Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2021
Language-Team: Spanish (https://www.transifex.com/anticapitalista/teams/10162/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Elija la zona horaria (con las flechas y la tecla Enter) Fecha: Ajustar fecha y hora Mueva la guía a la Hora correcta Mueva la guía al Minuto correcto Salir Seleccione la Zona Horaria Establecer la fecha correctal Ajustar la hora correcta Obtener la fecha y hora automáticamente de Internet 