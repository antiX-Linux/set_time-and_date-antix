��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �  �    9   �     
  %     &   7  &   ^     �     �     �     �  V   �                       	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:04+0000
Last-Translator: Manuel <senpai99@hotmail.com>, 2021
Language-Team: Spanish (Spain) (https://www.transifex.com/anticapitalista/teams/10162/es_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es_ES
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Elija la zona horaria (con las teclas del cursor y Enter) Fecha: Gestionar los ajustes de fecha y hora Mueva el deslizador a la hora correcta Mueve el deslizador al minuto correcto Salir Seleccione la zona horaria Ajustar la fecha actual Ajustar la hora actual Utilizar el servidor de hora de Internet para ajustar automáticamente la hora y fecha 