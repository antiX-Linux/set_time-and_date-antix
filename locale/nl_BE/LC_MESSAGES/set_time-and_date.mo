��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �      +   �     �  "   �  /   �  1   #     U     Z     m     �  E   �                       	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:04+0000
Last-Translator: Vanhoorne Michael, 2022
Language-Team: Dutch (Belgium) (https://www.transifex.com/anticapitalista/teams/10162/nl_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl_BE
Plural-Forms: nplurals=2; plural=(n != 1);
 Kies Tijdzone (met cursor en enter-toetsen) Datum: Datum- en tijdinstellingen beheren Verplaats de schuifregelaar naar het juiste uur Verplaats de schuifregelaar naar de juiste minuut Stop Selecteer tijdzone Huidige datum instellen Huidige tijd instellen Gebruik de internettijdserver om automatisch tijd/datum in te stellen 