��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �  p    <   �     �  !   �  '   �  *        G     O     e     |  J   �                       	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:04+0000
Last-Translator: Mehmet Akif 9oglu, 2022
Language-Team: Turkish (https://www.transifex.com/anticapitalista/teams/10162/tr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tr
Plural-Forms: nplurals=2; plural=(n > 1);
 Saat Dilimini seç (imleç ve enter tuşlarını kullanarak) Tarih: Tarih ve Saat Ayarlarını Yönet Kaydırıcıyı doğru Saate taşıyın Kaydırıcıyı doğru Dakikaya taşıyın Vazgeç Zaman Dilimini Seçin Geçerli Tarihi Ayarla Geçerli Saati Ayarla Saati/tarihi otomatik ayarlamak için İnternet Saati sunucusunu kullanın 