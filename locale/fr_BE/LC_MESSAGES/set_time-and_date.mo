��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �  �    E   �       +     +   E  (   q     �     �     �     �  H   �                       	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:04+0000
Last-Translator: Wallon Wallon, 2023
Language-Team: French (Belgium) (https://app.transifex.com/anticapitalista/teams/10162/fr_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_BE
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Choisir le fuseau horaire (en utilisant les touches curseur et Enter) Date: Gérer les paramètres de date et d’heure Déplacer le curseur sur l’heure correcte Déplacer le curseur sur la bonne minute Quitter Choisir le fuseau horaire Définir la date actuelle Régler l’heure actuelle Utiliser l’heure du serveur internet pour régler la date et l’heure 