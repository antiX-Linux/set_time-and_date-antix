��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �  s    ,   �     �  '   �     �          +     2     A     U  F   h                       	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:04+0000
Last-Translator: Gert Søgaard Jensen, 2023
Language-Team: Danish (https://www.transifex.com/anticapitalista/teams/10162/da/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: da
Plural-Forms: nplurals=2; plural=(n != 1);
 Vælg TidsZone (brug Pil- og Enter-tasterne) Dato: Håndtér indstillinger for Dato og Tid Flyt skyderen til korrekt Time Flyt skyderen til korrekt Minut Afslut Vælg TidsZone Indstil aktuel Dato Indstil aktuel Tid Brug en Internet Time Server til automatisk indstilling af Tid og Dato 